# Olertu's CS:GO configuration

The sole purpose of this project is to keep track of olertu's custom Counter-Strike:Global Offensive configuration and to share it.

You can find the most recent version of the necessary files [here](https://gitlab.com/olertu/cs_go_configuration/tree/master).
